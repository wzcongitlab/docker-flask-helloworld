FROM alpine:3.1

MAINTAINER Zhongchun Wang "zhongchun.wang@hpe.com"

RUN apk add --update python py-pip

RUN pip install Flask

COPY ./app /src/app

EXPOSE 5000

CMD ["python", "/src/app/main.py",  "-p 5000"]