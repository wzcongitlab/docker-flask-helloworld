import sys
from app import app
from optparse import OptionParser
 
@app.route('/')
@app.route('/index')
def index():
  return "Hello, World!(Docker-flask-helloworld Last updated: 10/24/2017 17:00:00)"

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-p', '--port', dest='app_port', action='store')
    (options, args) = parser.parse_args()
    if options.app_port == None:
        print "Missing required argument: -p/--port"
        sys.exit(1)
    
    print (options.app_port)   
    app.run(host="0.0.0.0", port=int(options.app_port))
